#ar-commonutils

##Versions
0.0.11

## Usage (in case you want to make the repo private) [1]:

- Add the project's private key (projectx_bitbucket) to the host machine that
  wants to clone this repo. The usual location would be &HOME/.ssh/projectx_bitbucket
  
- Add the following ssh configurations to the $HOME/.ssh/config file [2, 3]

    # --- For pip installing of ProjectX python libraries ---
    Host bitbucket-projectx
            HostName bitbucket.org
            IdentityFile /Users/ktp/.ssh/projectx_bitbucket
    #--------------------------------------------------------

- To manually install it

    $ pip install git+ssh://git@bitbucket-projectx/absorouteio/ar-commonutils.git@<Release_version>
    
- To include in the requirements.txt, add this line to it.

    git+ssh://git@bitbucket-projectx/absorouteio/ar-commonutils.git@<Release_version>
    
  where Release_version is the Release version tagged to the master branch.
  
## Usage (in case you want to make the repo public):

- No need to worry about the key file. To manually install it

    $ pip install git+https://bitbucket.org/absorouteio/ar-commonutils.git@<Release_version>
    
- To include in the requirements.txt, add this line to it.

    git+https://bitbucket.org/absorouteio/ar-commonutils.git@<Release_version>
    
  where Release_version is the Release version tagged to the master branch.
  
## References

[1] https://stackoverflow.com/questions/4830856/is-it-possible-to-use-pip-to-install-a-package-from-a-private-github-repository