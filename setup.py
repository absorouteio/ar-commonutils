from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'tensorflow==1.11.0',
    'tensorflow-serving-api==1.11.0',
    'Pillow==5.2.0',
    'kubernetes==7.0.0'
]

setup(name='ar-commonutils',
      version='0.0.12',
      url='https://bitbucket.org/absorouteio/ar-commonutils',
      packages=find_packages(exclude=('tests',)) + ['absoroute/data'],
      include_package_data=True,
      description='AbsoRoute common utils',
      author='Peeranat Fupongsiripan',
      author_email='peeranat@absoroute.io',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)

