#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 21:29:48 2018

@author: ktp
"""

import time
import unittest as ut
import absoroute.k8s_utils as k8s


class TestUtilsK8s(ut.TestCase):
    
    # TODO remove this dependency on actual Deployment
    _K8S_DEP_NAME = "dev-fas-proxy"
    
    
    @classmethod
    def setUpClass(cls):
        pass
        
        
    def test_get_deployment_status(self):
        deployments = k8s.get_deployment_status()
        
        self.assertTrue(len(deployments) > 0)
        
    
    def test_get_deployment_status_specific_name(self):
        deployments = k8s.get_deployment_status(self._K8S_DEP_NAME, ["ENV"])
        fas_proxy = deployments[0]
        
        self.assertTrue(len(deployments) > 0)
        self.assertTrue("NAME" in fas_proxy.keys())
        self.assertTrue("ENV" in fas_proxy.keys())
        
        
    @ut.skip("Takes too long. Do it when needed")
    def test_create_new_deployment_and_delete_deployment(self):
        DEP_NAME = "test-deployment"
        
        # create it
        k8s.create_new_deployment(
                DEP_NAME, {"ENV": "development-unittest"}, 
                "asia.gcr.io/projectx-204113/fr-searcher:latest", 3344, 0)
        time.sleep(30)
        
        # check if it's really there
        deployments = k8s.get_deployment_status(DEP_NAME, ["ENV"])
        test_deployment = deployments[0]
        print(deployments)
        
        self.assertTrue(len(deployments) == 1)
        self.assertTrue("NAME" in test_deployment.keys())
        self.assertTrue("ENV" in test_deployment.keys())
        self.assertEqual(test_deployment["ENV"], "development-unittest")
        
        # delete it
        k8s.delete_deployment(DEP_NAME)
        time.sleep(30)
        
        # check it's gone
        deployments = k8s.get_deployment_status(DEP_NAME, ["ENV"])
        print(deployments)
        
        self.assertTrue(len(deployments) == 0)
        
        
    def test_get_service_status(self):
        ret = k8s.get_service_status()
        items = ret["items"]
        self.assertTrue(len(items) > 0)
        
        ret = k8s.get_service_status(label="app=" + self._K8S_DEP_NAME)
        items = ret["items"]
        self.assertTrue(len(items) == 1)
        
        ret = k8s.get_service_status(label="app=invalid-service")
        items = ret["items"]
        self.assertTrue(len(items) == 0)
        
        
    def test_get_service_host(self):
        (host, port) = k8s.get_service_host(label="app=" + self._K8S_DEP_NAME)
#        print(host + ":" + str(port))
        
        self.assertTrue(host is not None)
        self.assertTrue(port is not None)
        
        
    def test_validate_service_name(self):
        self.assertEqual("abc-def.ghi", k8s.validate_service_name("aBc&DEF.ghi"))
        self.assertEqual("test-user", k8s.validate_service_name("test_usEr"))
        self.assertEqual("valid-service.name", k8s.validate_service_name("valid-service.name"))
        
        
if __name__ == '__main__':
    ut.main()
    
    
    