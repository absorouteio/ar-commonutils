#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PoC on google api client for kubernetes

==================

[1] seems to provide limited access to all K8S resources (only locations and 
zones to be specific). We can only manage to cluster / node level, but not with
Deployment or pods.

[2] is a commandline tool (kubectl) that can manage Deployment. To use this one
you have to issue commands to the kubectl via commandline manually

[3] is a python wrapper around [2]. But you need to set the $HOME/.kube/config
file to point to GG K8S first. This is seemed to be discussed in [4]. In summary, 
do the following steps

    - Let gcloud create initial configs by running the following command to get
      access credentials for the specified cluster and append them into 
      kubeconfig file. This will also set the default cluster kubectl will talk
      to later on. 
      
        $gcloud container clusters get-credentials <K8S cluster name> --zone asia-east1-b --project projectx-204113

      The config is by defaulted saved at $HOME/.kube/config

    - If you want to run kubectl against any specific cluster besides the default
      one, you can append option --cluster <K8S cluster name> to the command. 
      This is useful for, e.g., switching between development and prod cluster.

==================

References

[1] Google API client
https://developers.google.com/api-client-library/python/start/get_started

[2] Kubernetes API doc
https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#-strong-api-overview-strong-

[3] https://github.com/kubernetes-client/python

[4] https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl

Created on Sat Oct  6 15:20:37 2018

@author: ktp
"""

import os
import json

from kubernetes import client, config
from apiclient.discovery import build


# =========== Just testing the tools

API_NAME = "container"
API_VERSION = "v1"

# setting private key file for accessing K8S service
# see https://cloud.google.com/docs/authentication/production#obtaining_and_providing_service_account_credentials_manually 
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/ktp/workspace/ProjectX/FRPersistentManager/src/data/projectx-204113-c619b3479fe8.json"


service = build(API_NAME, API_VERSION)

parent = 'projects/projectx-204113/locations/asia-east1-b'
req = service.projects().locations().operations().list(parent=parent)
rsp = req.execute()
rsp_str = json.dumps(rsp, sort_keys=True, indent=4)
print(rsp_str)








# Currently this configfile contains only "development" cluster info. When we have prod, 
# we should generate another kubeconfig file and switch to reading the correct
# one depending on ENV variable.
config_file = "/Users/ktp/workspace/ProjectX/ARCommonUtils/absoroute/data/kubeconfig_dev"
config.load_kube_config(config_file)

k8s_client = client.CoreV1Api()
print("Listing pods with their IPs:")
ret = k8s_client.list_pod_for_all_namespaces(watch=False)
for i in ret.items:
    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

# ============ inspect all Services.
    
ret = k8s_client.list_service_for_all_namespaces(label_selector="app=dev-fr-searcher")
print("Listing services for all namespaces")

for i in ret.items:
    # The fields here correspond generally to the fields in deployment yaml file (not exactly)
    api_version = i.api_version
    kind = i.kind
    meta_data = i.metadata
    spec = i.spec
    ports = spec.ports
    status = i.status
    print("%s at clusterIP %s, port %i" % (meta_data.name, spec.cluster_ip, ports[0].port))

# ============ CRUD Deployment
    
# In our use case, we will have a FRSearcher container ready. However we will
# need to modify some env variables for each new FRSearcher deployment. In the 
# yaml file, it's in the spec.template.spec.containers.env part. In kubectl API
# it should be in the client.V1Container structure. See in the example below, 
# specifically in the create_deployment_object() method that builds deployment
# meta data object. 
    
# See 
# - https://github.com/kubernetes-client/python/blob/master/examples/deployment_examples.py
# - https://github.com/kubernetes-client/python/blob/master/examples/create_deployment.py
    

def create_deployment_and_service_objects():
    # Configureate Pod template container
    env_list = [
            client.V1EnvVar(name="ENV", value="development"),
            client.V1EnvVar(name="REDIS_PARTITION_KEY", value="0"),
            client.V1EnvVar(name="OPERATING_MODE", value="static")]
    
    resource_requirements = client.models.V1ResourceRequirements(
            limits={"memory": "2200Mi"}, requests={"memory": "1800Mi"})
    
    container = client.V1Container(
            name="dev-poc-fr-searcher",
            image="asia.gcr.io/projectx-204113/fr-searcher:latest",
            ports=[client.V1ContainerPort(container_port=2047)],
            resources=resource_requirements,
            env=env_list)
    
#    container = client.V1Container(
#            name="dev-poc-fr-searcher",
#            image="asia.gcr.io/projectx-204113/fr-searcher:latest",
#            ports=[client.V1ContainerPort(container_port=2047)],
#            env=env_list)
    
    # Create and configurate a spec section
    template = client.V1PodTemplateSpec(
            metadata=client.V1ObjectMeta(labels={"app": "dev-poc-fr-searcher"}),
            spec=client.V1PodSpec(containers=[container]))
    
    # Create the specification of deployment
    # TODO selector ?
    spec = client.ExtensionsV1beta1DeploymentSpec(
            replicas=1,
            template=template)
    
    # Instantiate the deployment object
    deployment_obj = client.ExtensionsV1beta1Deployment(
            api_version="extensions/v1beta1",
            kind="Deployment",
            metadata=client.V1ObjectMeta(name="dev-poc-fr-searcher"),
            spec=spec)
    
    # Service
    service_meta = client.V1ObjectMeta(
            name="dev-poc-fr-searcher",
            namespace="default",
            labels={"app": "dev-poc-fr-searcher"})
    
    service_spec = client.V1ServiceSpec(
            selector={"app": "dev-poc-fr-searcher"},
            ports=[client.V1ServicePort(port=80, target_port=2047)],
            type="ClusterIP")
    
    service_obj = client.V1Service(
            api_version="v1",
            kind="Service",
            metadata=service_meta,
            spec=service_spec)

    return (deployment_obj, service_obj)


def create_deployment(api_instance, deployment):
    # Create deployement
    api_response = api_instance.create_namespaced_deployment(
        body=deployment,
        namespace="default")
    print("Deployment created. status='%s'" % str(api_response.status))


def update_deployment(api_instance, deployment):
    # Update the deployment
    api_response = api_instance.patch_namespaced_deployment(
        name="dev-poc-fr-searcher",
        namespace="default",
        body=deployment)
    print("Deployment updated. status='%s'" % str(api_response.status))


def delete_deployment(api_instance):
    # Delete deployment
    api_response = api_instance.delete_namespaced_deployment(
        name="dev-poc-fr-searcher",
        namespace="default",
        body=client.V1DeleteOptions(
            propagation_policy='Foreground',
            grace_period_seconds=0))
    print("Deployment deleted. status='%s'" % str(api_response.status))
    

k8s_client_beta = client.ExtensionsV1beta1Api()

# ============ inspect all Deployments.
    
ret = k8s_client_beta.list_deployment_for_all_namespaces()
print("Listing deploymnents for all namespaces")

for i in ret.items:
    name = i.metadata.name
    print(name)
    
    if name == "dev-poc-fr-searcher":
        envs = i.spec.template.spec.containers[0].env
        print(envs)
    
# ============ CRUD deployment / service

(deployment_obj, service_obj) = create_deployment_and_service_objects()
create_deployment(k8s_client_beta, deployment_obj)

ret = k8s_client.create_namespaced_service(namespace="default", body=service_obj, pretty=True)



env_list = [
    client.V1EnvVar(name="ENV", value="development"),
    client.V1EnvVar(name="REDIS_PARTITION_KEY", value="0"),
    client.V1EnvVar(name="OPERATING_MODE", value="dynamic")]
deployment_obj.spec.template.spec.containers[0].env=env_list
update_deployment(k8s_client_beta, deployment_obj)


# TODO delete service as well
delete_deployment(k8s_client_beta)

delete_service_body = client.V1DeleteOptions(
        api_version="v1", grace_period_seconds=0, propagation_policy='Foreground')
ret = k8s_client.delete_namespaced_service(
        name="dev-poc-fr-searcher", namespace="default", body=delete_service_body, 
        pretty=True)







# Example deployment yaml file
    
#apiVersion: extensions/v1beta1
#kind: Deployment
#metadata:
#  annotations:
#    deployment.kubernetes.io/revision: "5"
#  creationTimestamp: 2018-10-04T14:30:34Z
#  generation: 12
#  labels:
#    app: dev-fas-proxy
#  name: dev-fas-proxy
#  namespace: default
#  resourceVersion: "407228"
#  selfLink: /apis/extensions/v1beta1/namespaces/default/deployments/dev-fas-proxy
#  uid: 0de8c73e-c7e2-11e8-8fa8-42010af0023b
#spec:
#  replicas: 0
#  selector:
#    matchLabels:
#      app: dev-fas-proxy
#  strategy:
#    rollingUpdate:
#      maxSurge: 1
#      maxUnavailable: 1
#    type: RollingUpdate
#  template:
#    metadata:
#      creationTimestamp: null
#      labels:
#        app: dev-fas-proxy
#    spec:
#      containers:
#      - env:
#        - name: ENV
#          valueFrom:
#            configMapKeyRef:
#              key: ENV
#              name: dev-fas-proxy-config
#        - name: FA_CONN_FR_HOST
#          valueFrom:
#            configMapKeyRef:
#              key: FA_CONN_FR_HOST
#              name: dev-fas-proxy-config
#        - name: FA_CONN_GP_HOST
#          valueFrom:
#            configMapKeyRef:
#              key: FA_CONN_GP_HOST
#              name: dev-fas-proxy-config
#        - name: FA_CONN_AP_HOST
#          valueFrom:
#            configMapKeyRef:
#              key: FA_CONN_AP_HOST
#              name: dev-fas-proxy-config
#        - name: FA_CONN_EP_HOST
#          valueFrom:
#            configMapKeyRef:
#              key: FA_CONN_EP_HOST
#              name: dev-fas-proxy-config
#        image: asia.gcr.io/projectx-204113/fas-proxy:21fda47054d22120b08201c6bbe7090d264f472f
#        imagePullPolicy: Always
#        name: fas-proxy
#        resources: {}
#        terminationMessagePath: /dev/termination-log
#        terminationMessagePolicy: File
#      dnsPolicy: ClusterFirst
#      restartPolicy: Always
#      schedulerName: default-scheduler
#      securityContext: {}
#      terminationGracePeriodSeconds: 30
#status:
#  conditions:
#  - lastTransitionTime: 2018-10-04T14:31:18Z
#    lastUpdateTime: 2018-10-04T14:31:18Z
#    message: Deployment has minimum availability.
#    reason: MinimumReplicasAvailable
#    status: "True"
#    type: Available
#  observedGeneration: 12




