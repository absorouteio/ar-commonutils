import os
import numpy as np
import unittest as ut

from absoroute import common_utils

dir = os.path.dirname(os.path.realpath(__file__))


class CommonUtilsTest(ut.TestCase):

    def test_get_current_time_ms(self):
        self.assertTrue(isinstance(common_utils.get_current_time_ms(), float))

    def test_build_numpy_array_image(self):
        with open(dir + '/plane.jpg', 'rb') as f:
            out = common_utils.build_numpy_array_image(f, dtype=np.int)
            self.assertEqual(out.shape, (360, 480, 3))
            self.assertEqual(out.dtype, np.int)

    def test_build_numpy_array_image_resize(self):
        with open(dir + '/plane.jpg', 'rb') as f:
            out = common_utils.build_numpy_array_image(f, size=(200, 200), dtype=np.int)
            self.assertEqual(out.shape, (200, 200, 3))
            self.assertEqual(out.dtype, np.int)

    def test_build_numpy_array_image_resize_throw_exception(self):
        with open(dir + '/plane.jpg', 'rb') as f:
            self.assertRaises(Exception, common_utils.build_numpy_array_image, f, size=22, dtype=np.int)

    def test_build_numpy_array_images(self):
        file = dir + '/plane.jpg'
        imgs = [open(file, 'rb'), open(file, 'rb')]
        out = common_utils.build_numpy_array_images(imgs, dtype=np.float32)
        self.assertEqual(len(out), 2)
        self.assertEqual(out[0].shape, (360, 480, 3))
        self.assertEqual(out[0].dtype, np.float32)


if __name__ == '__main__':
    ut.main()