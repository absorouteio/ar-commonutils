import io
import time
import numpy as np
from PIL import Image


def get_current_time_ms():
    """returns current time in millisecond"""
    return time.time() * 1000


def build_numpy_array_image(image, size=None, dtype=None):
    """returns image as numpy array

    Parameters
    ----------
    image : bytearray
        bytearray of an image.

    size : size in pixels, as a 2-tuple, default = None
        (width, height)

    dtype : DataType
        the preferred data type. If None, the data type is automatically inferred.
    """
    img = Image.open(io.BytesIO(image.read()))
    img = img.resize(size) if size else img
    return np.asarray(img, dtype=dtype)


def build_numpy_array_images(images, size=None, dtype=None):
    """returns a list of images as numpy array

    Parameters
    ----------
    images : list
        list of bytearray of images.

    size : size in pixels, as a 2-tuple, default = None
        (width, height)

    dtype : DataType, default = None
        the preferred data type. If None, the data type is automatically inferred.
    """
    return [build_numpy_array_image(img_bytes, size=size, dtype=dtype) for img_bytes in images]
