import grpc
import tensorflow as tf
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2_grpc, get_model_status_pb2, model_service_pb2_grpc


def build_predict_request(inputs, model_name, signature):
    """builds TensorFlow PredictRequest instance.

    Parameters
    ----------
    inputs : dict
        a dictionary containing inputs to TensorFlow model server. Each key corresponds to the one defined
        in TensorFlow Serving model signature where its corresponding value is the feature. e.g.

        inputs = {
            'images': np.array(images)
        }

    model_name : str
        name of the model deployed in TensorFlow model server.

    signature : str
        a signature name defined in TensorFlow Serving. The model signature defines inputs, outputs and type of
        prediction.
    """
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model_name
    request.model_spec.signature_name = signature

    for alias, X in inputs.items():
        request.inputs[alias].CopyFrom(tf.make_tensor_proto(X))

    return request


class TfModel:
    """TensorFlow Serving client. It provides a straightforward gRPC communication between Python and TensorFlow
    Serving system.

    Parameters
    ----------
    host : str
        The name of the remote host to which to connect.

    port : str
        The port of the remote host to which to connect.
    """
    def __init__(self, host, port):
        addr = str(host) + ':' + str(port)
        self.channel = grpc.insecure_channel(addr)
        self.stub = prediction_service_pb2_grpc.PredictionServiceStub(self.channel)

    def get_model_status(self, model_spec, timeout=5.0):
        """returns model status.

        Parameters
        ----------
        model_spec : str
            name of the model deployed in TensorFlow model server.

        timeout : float, default 5.0
            request timeout.

        Returns
        -------
        out: dict
            a dictionary containing information about model_spec, status, and version of the model.
        """
        request = get_model_status_pb2.GetModelStatusRequest()
        request.model_spec.name = model_spec

        stub = model_service_pb2_grpc.ModelServiceStub(self.channel)
        response = stub.GetModelStatus(request, timeout)
        model_version = response.model_version_status[0].version
        # we are only interested in status == 30 meaning the model is AVAILABLE,
        # see https://github.com/tensorflow/serving/blob/master/tensorflow_serving/apis/get_model_status.proto
        # for more status
        model_status = 'AVAILABLE' if response.model_version_status[0].state == 30 else 'UNAVAILABLE'
        return {'model_spec': model_spec, 'status': model_status, 'version': model_version}

    def predict_request(self, request, timeout=5.0):
        """sends predict request to TensorFlow model server.

        Parameters
        ----------
        request : PredictRequest
            TensorFlow PredictRequest instance.

        timeout : float, default 5.0
            request timeout.
        """
        pred = self.stub.Predict(request, timeout)
        return pred
