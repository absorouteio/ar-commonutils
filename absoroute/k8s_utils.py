#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Environment variables needed by this utils
- LOG_LEVEL
- APPLOG_LEVEL
- KUBECTL_CONFIG_FILE
- GOOGLE_CRED_FILE

Created on Mon Oct 15 21:36:17 2018

@author: ktp
"""

import re
import os
import absoroute.data as data
import absoroute.data.settings_k8s_utils as sku

from kubernetes import client, config
from arcommon.applog_utils import AppLogger


DEPLOYMENT_API_VERSION = "extensions/v1beta1"
SERVICE_API_VERSION = "v1"
DEL_PROPAGATION_POLICY = "Foreground"
DEL_GRACE_PERIOD = 0


_logger = AppLogger(name=__name__, applog_level=sku.APPLOG_LEVEL)


def _load_credentials():
    """
    Set the environment variables GOOGLE_APPLICATION_CREDENTIALS to point to GC 
    credentials file. This variable will be used by google's api.
    """
    _root_path = data.__path__[0]
    _credential_file = sku.GOOGLE_CRED_FILE
    
    if not os.path.isabs(_credential_file):
        _credential_file = os.path.join(_root_path, _credential_file)
    
    _logger.debug("Using Google credential file %s" % _credential_file)
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = _credential_file
    
    # Set the kubectl config file
    _config_file = sku.KUBECTL_CONFIG_FILE
    
    if not os.path.isabs(_config_file):
        _config_file = os.path.join(_root_path, _config_file)
    
    _logger.debug("Using config file %s for kubectl" % _config_file)
    config.load_kube_config(_config_file)


# Note that calling _load_credentials() everytime we get a new client seems
# overkill. But it might just solve the issue of getting 401 back after a while
def _get_k8s_client():
    _load_credentials()
    return client.CoreV1Api()


def _get_k8s_client_beta():
    _load_credentials()
    return client.ExtensionsV1beta1Api()


def get_deployment_status(base_name=None, env_list=[]):
    """
    A util to get statuses of all Deployments (partially) matching the given name
    and return as a list of maps.
    
    Params:
        base_name - Name, or part of the name of the interested Deployments. If
                    None, then get all Deployments.
        env_list - List of environment variabls set for the Deployment that should 
                be included in the returned results
    Returns:
        a list of maps, each containing keys "NAME" and those provided in env_list
    """
    
    start_ts = _logger.debug("Getting Deployment statuses matching base name %s, with envs %s" % (base_name, env_list))
    ret = _get_k8s_client_beta().list_deployment_for_all_namespaces()
    statuses = []
    
    for deployment in ret.items:
        name = deployment.metadata.name
#        _logger.debug("Found Deploymemnt %s" % name)
        
        if (base_name == None) or ((base_name != None) and (base_name in name)):
            status = {}
            status["NAME"] = name
            envs = deployment.spec.template.spec.containers[0].env
            
            if envs != None:
                for env in envs:
                    if env.name in env_list:
                        status[env.name] = env.value
                        
            statuses.append(status)
        
    _logger.debug("Found matching Deployment %s" % statuses, begin_time=start_ts)
    return statuses


def get_service_status(label=None):
    """
    Get service status
    
    Params:
        label   - If given, filter only services with the given label. The label
                  is of the form "<key>=<value>"
    Returns:
        A dict of the returned result from list_service_for_all_namespaces()
    """
    if label:
        return _get_k8s_client().list_service_for_all_namespaces(
                label_selector=label, pretty=True).to_dict()
    else:
        return _get_k8s_client().list_service_for_all_namespaces(pretty=True).to_dict()
    
    
def get_service_host(label):
    """
    Get the cluster_ip / port of a service with the given label
    
    Params:
        label   - Filter only services with the given label. The label is of the 
                  form "<key>=<value>"
    Returns:
        (host, port) where host and port are str and int, or (None, None) if no 
        service is found
    """
    service_status = get_service_status(label)
    items = service_status["items"]
    
    if len(items) != 1:
        _logger.warning("Found %i services matching label %s" % (len(items), label))
        return (None, None)
    
    cluster_ip = items[0]["spec"]["cluster_ip"]
    port = int(items[0]["spec"]["ports"][0]["port"])
    
    return (cluster_ip, port)
    

def _create_deployment_and_service_object(
        deployment_name, deployment_envs, namespace, image, replicas, 
        container_port, service_port, service_type="ClusterIP", 
        rsc_limits=None, rsc_requests=None):
    """
    Create deployment and service objects
    
    Params:
        rsc_limits - a map between resource and limited amount, e.g., {"memory": "100Mi"},
                     as max limit for the deployment
        rsc_requests - a map between resource and requested amount, e.g., 
                     {"memory": "200Mi"}, as requested amount for the deployment
    """
    labels_map = {"app": deployment_name}
    
    # ======================== Deployment object =============================
    
    env_list = []
    
    for (k, v) in deployment_envs.items():
        # This will be jsonified. Need to make sure both k and v are str
        env_list.append(client.V1EnvVar(name=str(k), value=str(v)))
        
    resource_requirements = client.models.V1ResourceRequirements(
            limits=rsc_limits, requests=rsc_requests)
        
    container = client.V1Container(
            name=deployment_name, image=image, env=env_list, 
            resources=resource_requirements,
            ports=[client.V1ContainerPort(container_port=container_port)])
    
    template = client.V1PodTemplateSpec(
            metadata=client.V1ObjectMeta(labels=labels_map),
            spec=client.V1PodSpec(containers=[container]))
    
    spec = client.ExtensionsV1beta1DeploymentSpec(
            replicas=replicas, 
            template=template)
    
    deployment = client.ExtensionsV1beta1Deployment(
            api_version=DEPLOYMENT_API_VERSION, 
            kind="Deployment", 
            spec=spec,
            metadata=client.V1ObjectMeta(name=deployment_name))
    
    # ========================= Service object ===============================
    
    service_meta = client.V1ObjectMeta(
            name=deployment_name, namespace=namespace, labels=labels_map)
    
    service_spec = client.V1ServiceSpec(
            selector=labels_map, type=service_type, ports=[
                    client.V1ServicePort(port=service_port, target_port=container_port)])
    
    service = client.V1Service(
            api_version=SERVICE_API_VERSION, kind="Service", metadata=service_meta,
            spec=service_spec)

    return (deployment, service)


def validate_service_name(name):
    """
    Validate the given Deployment name obeys restriction placed by K8S (only lower 
    case alphanumeric characters, '-' or '.'). If the given name violates the 
    restriction, a warning is logged and it'll try its best to modify the given
    name to meet the requirement.
    
    Returns:
        A valid K8S Deployment name.
    """
    matcher = re.compile("[^.a-z0-9-]+")
    matches = matcher.findall(name)
    
    if matches:
        _logger.warning("Given Deployment name %s violates K8S's restriction." % name)
        name = name.lower()
        
        # find the matches again, to exclude capital letters away from the matches
        matches = matcher.findall(name)
        
        for match in matches:
            name = name.replace(match, "-")
            
        _logger.warning("Deployment name changed to %s" % name)
        
    return name
    

def create_new_deployment_and_service(
        name, env_map, image, container_port, service_port, replicas, 
        namespace="default", rsc_limits=None, rsc_requests=None):
    """
    A util to creage a new Deployment / Service with the given name and 
    environment variables in the env_list.
    
    Params:
        name    - Name of the deployment (lower case alphanumeric characters, '-' or '.')
        env_map - A map between environment variable's name and its value 
        container_port - Application's listening port
        service_port - Service port to the outside world that is to be mapped to 
                       container_port
        rsc_limits - a map between resource and limited amount, e.g., 
                     {"memory": "100Mi"}, as max limit for the deployment
        rsc_requests - a map between resource and requested amount, e.g., 
                     {"memory": "200Mi"}, as requested amount for the deployment
    """
    name = validate_service_name(name)
    _logger.debug("Creating a new Deployment / Service %s with envs %s" % (name, env_map))
    (deployment_obj, service_obj) = _create_deployment_and_service_object(
            name, env_map, namespace, image, replicas, container_port, 
            service_port, rsc_limits=rsc_limits, rsc_requests=rsc_requests)
    
    dep_resp = _get_k8s_client_beta().create_namespaced_deployment(
            body=deployment_obj, namespace=namespace, pretty=True)
    _logger.debug("Create Deployment response %s" % str(dep_resp.status))
    
    serv_resp = _get_k8s_client().create_namespaced_service(
            namespace=namespace, body=service_obj, pretty=True)
    _logger.debug("Create Service response %s" % str(serv_resp.status))
    
    
def update_deployment(name, deployment_obj, namespace="default"):
    
    _logger.debug("Updating a Deployment %s with deployment_obj %s" % (name, deployment_obj))
    api_response = _get_k8s_client_beta().patch_namespaced_deployment(
        name=name, namespace=namespace, body=deployment_obj)
    _logger.debug("Got response %s" % str(api_response.status))
    
    
def delete_deployment_and_service(name, namespace="default"):
    """
    A util to delete a Deployment / Service with the given name and namespace
    """
    _logger.debug("Deleting a Deployment / Service %s" % (name))
    
    dep_resp = _get_k8s_client_beta().delete_namespaced_deployment(
            name=name, namespace=namespace, pretty=True,
            body=client.V1DeleteOptions(
                propagation_policy=DEL_PROPAGATION_POLICY,
                grace_period_seconds=DEL_GRACE_PERIOD))
    _logger.debug("Delete Deployment response %s" % str(dep_resp.status))
    
    delete_service_body = client.V1DeleteOptions(
            api_version=SERVICE_API_VERSION, grace_period_seconds=DEL_GRACE_PERIOD, 
            propagation_policy=DEL_PROPAGATION_POLICY)
    serv_resp = _get_k8s_client().delete_namespaced_service(
            name=name, namespace=namespace, body=delete_service_body, pretty=True)
    _logger.debug("Delete Service response %s" % str(serv_resp.status))





