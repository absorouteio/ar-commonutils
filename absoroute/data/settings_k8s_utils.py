#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 17:26:20 2019

@author: ktp
"""

import os


# logging
LOG_LEVEL = int(os.getenv("LOG_LEVEL", 10))             # 10 - Debug, 40 - Error
APPLOG_LEVEL = int(os.getenv("APPLOG_LEVEL", 10))       # 10 - Debug, 40 - Error

# default credential files
# Set the environment variables GOOGLE_CRED_FILE to point to GC 
# credentials file, and the KUBECTL_CONFIG_FILE to point to K8S config file. 
# Both must be absolute paths, otherwise it's expected that the given paths 
# are relative from the absoroute/data folder.
KUBECTL_CONFIG_FILE = os.getenv("KUBECTL_CONFIG_FILE", "kubeconfig_dev") 
GOOGLE_CRED_FILE = os.getenv("GOOGLE_CRED_FILE", "projectx-204113-c619b3479fe8.json")